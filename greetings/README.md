# Set up an advanced hardhat project

This project demonstrates an advanced Hardhat use case, integrating other tools commonly used alongside Hardhat in the ecosystem.

The project comes with a contract, a test for that contract, a script that deploys that contract, and a task implementation, which simply lists the available accounts. It also comes with a variety of other tools, preconfigured to work with the project code.

Try running some of the following tasks:

```shell
npx hardhat
npm install hardhat
npm install dotenv
npm install @nomiclabs/hardhat-etherscan
npm install @nomiclabs/hardhat-waffle
npm install hardhat-gas-reporter
npm install solidity-coverage
npx hardhat accounts --show-stack-traces
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
npx hardhat help
set REPORT_GAS=true
npx hardhat test
npx hardhat coverage
npx hardhat run scripts/deploy.js
node scripts/deploy.js
npm install eslint
npm install standard
npm install --save-dev eslint-config-prettier
npm install --save-dev eslint-plugin-node
# npx eslint '**/*.js'
npx eslint scripts\deploy.js
npm install chai
npx eslint test/*.js --fix
npm install prettier-plugin-solidity
npx prettier **/*.{json,sol,md} --check
npx prettier **/*.{json,sol,md} --write
npm install solhint
npx solhint contracts/**/*.sol solidity-coverage
npx solhint contracts/**/*.sol --fix
```

# Etherscan verification

To try out Etherscan verification, you first need to deploy a contract to an Ethereum network that's supported by Etherscan, such as Ropsten.

In this project, copy the .env.example file to a file named .env, and then edit it to fill in the details. Enter your Etherscan API key, your Ropsten node URL (eg from Alchemy), and the private key of the account which will send the deployment transaction. With a valid .env file in place, first deploy your contract:

```shell
hardhat run --network ropsten scripts/deploy.js
```
C:\projects\hardhat\greetings>npx hardhat run scripts/deploy.js                   
Deploying a Greeter with greeting: Hello, Hardhat!
Greeter deployed to: 0x5FbDB2315678afecb367f032d93F642f64180aa3

Then, copy the deployment address and paste it in to replace `DEPLOYED_CONTRACT_ADDRESS` in this command:

```shell
npx hardhat verify --network ropsten 0x167C5F320e068a203408693a8cb256CB7Aa80321 "Hello, Hardhat!"
```

C:\projects\hardhat\greetings>npx hardhat verify --network ropsten 0x167C5F320e068a203408693a8cb256CB7Aa80321 "Hello, Hardhat!"
Nothing to compile
Successfully submitted source code for contract
contracts/Greeter.sol:Greeter at 0x167C5F320e068a203408693a8cb256CB7Aa80321
for verification on the block explorer. Waiting for verification result...

Error in plugin @nomiclabs/hardhat-etherscan: The Etherscan API responded with a failure status.
The verification may still succeed but should be checked manually.
Reason: Already Verified
